jQuery(document).ready(function ($) {
	if ($('.list-table').length) {
		let data = {
			action: 'get_tables',
			nonce: iwp.nonce,
		}
		
		$.ajax({
			type: 'POST',
			url: iwp.ajaxUrl,
			data: data,
			success: function (res) {
				if (res.success) {
					let headline = res.data.data.title;
					let tableHead = res.data.data.data.headers;
					let tableRow = res.data.data.data.rows;
					
					// add text headline
					$('#title').text(headline);
					
					// add head of table
					$.each(tableHead, function (i, val) {
						let html = `<th scope="col">${val}</th>`;
						$('.list-table #header').append(html);
					});
					
					//add rows of table
					$.each(tableRow, function (i, el) {
						$('.list-table tbody').append(`<tr data-index="${i}"></tr>`);
						$.each(el, function (indexEl, val) {
							let html = `<td>${val}</td>`;
							if ('date' === indexEl) {
								let date = new Date(val * 1000);
								let html = `<td>${date.toDateString()}</td>`;
								$(`.list-table tbody tr[data-index=${i}]`).append(html);
							} else {
								$(`.list-table tbody tr[data-index=${i}]`).append(html);
							}
						});
					});
				} else {
					Swal.fire({
						icon: 'error',
						title: res.data.title,
						text: res.data.message,
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			},
		});
	}
});