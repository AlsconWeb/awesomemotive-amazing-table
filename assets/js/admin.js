jQuery(document).ready(function ($) {
	$('.update-info').click(function (e) {
		e.preventDefault();
		
		let data = {
			action: 'get_tables_to_admin',
			update: true,
		}
		
		$.ajax({
			type: 'POST',
			url: iwp.ajaxUrl,
			data: data,
			success: function (res) {
				if (res.success) {
					let tableRow = res.data.data.data.rows;
					$('.table-info tbody tr').remove()
					
					//add rows of table
					$.each(tableRow, function (i, el) {
						$('.table-info tbody').append(`<tr data-index="${i}"></tr>`);
						$.each(el, function (indexEl, val) {
							let html = `<td><p>${val}</p></td>`;
							if ('date' === indexEl) {
								let date = new Date(val * 1000);
								let html = `<td><p>${date.toDateString()}</p></td>`;
								$(`.table-info tbody tr[data-index=${i}]`).append(html);
							} else {
								$(`.table-info tbody tr[data-index=${i}]`).append(html);
							}
						});
					});
				} else {
					Swal.fire({
						icon: 'error',
						title: res.data.title,
						text: res.data.message,
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			},
		});
	})
});