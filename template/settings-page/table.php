<?php
/**
 * @package IWP
 *
 * Created 22.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

global $amazing_table;

$table_info = $amazing_table->get_table_info();
?>

<div class="amazing-table-content">
	<h1 class="screen-reader-text"><?php echo esc_html( $table_info['title'] ); ?></h1>
	<div class="amazing-table-content-headline">
		<h1 class="centered"><?php echo esc_html( $table_info['title'] ); ?></h1>
	</div>
	<div class="table-info">
		<?php if ( ! empty( $table_info['data'] ) ) : ?>
			<div class="amazing-table-admin-columns">
				<?php foreach ( $table_info['data']['headers'] as $head ) : ?>
					<div class="amazing-table-admin-column-20">
						<h3>
							<?php echo esc_html( $head ); ?>
						</h3>
					</div>
				<?php endforeach; ?>
			</div>
			<table>
				<tbody>
				<?php foreach ( $table_info['data']['rows'] as $row ) : ?>
					<tr>
						<?php foreach ( $row as $key => $cell ) : ?>
							<?php if ( 'date' === $key ) : ?>
								<td><?php echo esc_html( gmdate( 'D M n Y', $cell ) ); ?></td>
							<?php else : ?>
								<td><p><?php echo esc_html( $cell ); ?></p></td>
							<?php endif; ?>
						<?php endforeach; ?>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif; ?>
	</div>
	<div class="amazing-table-update-section">
		<div class="amazing-table-upsell-button">
			<a class="btn btn-lg btn-orange update-info" href="#"><?php esc_html_e( 'Update Info', 'iwp' ); ?></a>
		</div>
	</div>
</div>
