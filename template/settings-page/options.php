<?php
/**
 * Created 20.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP
 */

do_settings_sections( 'iwp-info-table' );
