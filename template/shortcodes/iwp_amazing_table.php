<?php
/**
 * @package  test_part_one
 *
 * Created 18.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

?>
<div class="list-table">
	<h1 id="title"></h1>
	<table class="table">
		<thead>
		<tr id="header">

		</tr>
		</thead>
		<tbody>

		</tbody>
	</table>
</div>