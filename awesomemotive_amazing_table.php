<?php
/**
 * @package  amazing_table
 *
 * Plugin Name: Awesomemotive Amazing table
 * Plugin URI: https://awesomemotive.com/career/developer-applicant-challenge/
 * Description: simple plugin
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 * License: GPLv2 or later
 * Text Domain: iwp
 */

use IWP\IWPMain;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'IWP_TEST_VERSION', '1.0.0' );
define( 'IWP_TEST_PLUGIN_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'IWP_TEST_PLUGIN_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );

require_once IWP_TEST_PLUGIN_DIR . '/vendor/autoload.php';


global $amazing_table;
$amazing_table = new IWPMain();
$amazing_table->init();

register_activation_hook( __FILE__, 'iwp_plugin_activation' );
/**
 * Activation Plugin.
 */
function iwp_plugin_activation() {
	global $amazing_table;

	$amazing_table->activation();
}

register_uninstall_hook( __FILE__, 'iwp_plugin_delete' );
/**
 * Delete Plugin
 */
function iwp_plugin_delete() {
	delete_transient( 'iwp_tables' );
	delete_option( 'iwp_time_requests' );
}
