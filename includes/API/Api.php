<?php
/**
 * Created 21.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\API
 */

namespace IWP\API;

/**
 * Class IWPApi
 */
class Api {
	/**
	 * Url endpoint from awesomemotive test api.
	 *
	 * @var string Url Api endpoint.
	 */
	private $url;

	/**
	 * Time after which the temporary option will be removed
	 *
	 * @var int
	 */
	private $time_update_table;

	/**
	 * IWPApi constructor.
	 */
	public function __construct() {

		$this->url               = 'https://miusage.com/v1/challenge/1/';
		$this->time_update_table = get_option( 'iwp_time_requests', true );
	}

	/**
	 * Receives data from Api.
	 *
	 * @return array|false
	 */
	public function get_api_request() {
		$response = wp_remote_get(
			$this->url,
			[
				'sslverify' => false,
				'timeout'   => 20,
			]
		);
		if ( ! is_wp_error( $response ) ) {
			$option_set = set_transient(
				'iwp_tables',
				json_decode( $response['body'], true, 512, JSON_THROW_ON_ERROR ),
				$this->time_update_table
			);

			if ( false === $option_set ) {
				return false;
			}
		}

		return $response;
	}

	/**
	 * Get tables.
	 *
	 * @param bool $force_update Flag to update data or not.
	 *
	 * @return false|mixed
	 */
	private function get_table( bool $force_update ) {
		$tables = '';
		if ( false === $force_update ) {
			$tables = get_transient( 'iwp_tables' );
		}

		if ( false === $tables || true === $force_update ) {
			$response = $this->get_api_request();
			if ( false === $response ) {
				return false;
			}
			$tables = json_decode( $response['body'], true, 512, JSON_THROW_ON_ERROR );
		}

		usort(
			$tables['data']['rows'],
			static function ( $a, $b ) {
				return $a['id'] <=> $b['id'];
			}
		);

		return $tables;
	}
}
