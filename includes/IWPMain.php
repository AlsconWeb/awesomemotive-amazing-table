<?php
/**
 * Created 17.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP
 */

namespace IWP;

use IWP\Admin\Page\SettingsPage;
use IWP\API\Api;
use IWP\CLI\WPCli;

/**
 * Class IWPMain.
 */
class IWPMain {

	/**
	 * Class IWPWpCli create custom command.
	 *
	 * @var WPCli
	 */
	private $cli;

	/**
	 * Works with awesomemotive test API
	 *
	 * @var Api
	 */
	private $api;

	/**
	 * IWPMain constructor.
	 */
	public function __construct() {
		if ( defined( 'WP_CLI' ) && constant( 'WP_CLI' ) ) {
			$this->cli = new WPCli();
		}
		$this->time_update_table = get_option( 'iwp_time_requests', true );
		new SettingsPage();

		$this->api = new Api();

		add_action( 'admin_enqueue_scripts', [ $this, 'add_script_to_admin_page' ] );
	}

	/**
	 * Initial Plugins.
	 */
	public function init(): void {
		add_action( 'wp_ajax_get_tables', [ $this, 'get_ajax_table_info' ] );
		add_action( 'wp_ajax_nopriv_get_tables', [ $this, 'get_ajax_table_info' ] );

		add_action( 'wp_ajax_get_tables_to_admin', [ $this, 'get_ajax_table_info' ] );
		add_action( 'wp_ajax_nopriv_get_tables_to_admin', [ $this, 'get_ajax_table_info' ] );

		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts' ] );

		add_shortcode( 'iwp_amazing_table', [ $this, 'output_shortcode' ] );

		if ( defined( 'WP_CLI' ) && constant( 'WP_CLI' ) ) {
			try {
				/**
				 * Method WP_CLI::add_command() accepts class as callable.
				 */
				\WP_CLI::add_command( 'iwp', $this->cli );
			} catch ( Exception $ex ) {
				return;
			}
		}
	}

	/**
	 * Returns table data
	 */
	public function get_ajax_table_info(): void {
		if ( isset( $_POST['nonce'] ) && ! wp_verify_nonce( wp_unslash( $_POST['nonce'] ), 'amazing_table' ) ) {
			wp_send_json_error(
				[
					'title'   => __( 'Oops...', 'iwp' ),
					'message' => __( 'Nonce code is wrong', 'iwp' ),
				]
			);
		}

		if ( ! isset( $_POST['update'] ) || 'true' !== $_POST['update'] ) {
			$tables = $this->api->get_table( false );
		} else {
			$tables = $this->api->get_table( true );
		}

		if ( false === $tables ) {
			wp_send_json_error(
				[
					'title'   => __( 'Oops...', 'iwp' ),
					'message' => __( 'Not have response from the API', 'iwp' ),
				]
			);
		}

		wp_send_json_success( [ 'data' => $tables ] );

	}

	/**
	 * Get Table info from admin Page.
	 *
	 * @return false|mixed
	 */
	public function get_table_info() {
		return $this->api->get_table( false );
	}

	/**
	 * Add script adn style in front-end.
	 */
	public function add_scripts(): void {
		wp_enqueue_script( 'sweetalert2', '//cdn.jsdelivr.net/npm/sweetalert2@11', [ 'jquery' ], '2.0.0', true );
		wp_enqueue_script( 'main', IWP_TEST_PLUGIN_URL . '/assets/js/main.js', [ 'jquery' ], $this->version( '/assets/js/main.js' ), true );

		wp_localize_script( 'main', 'iwp', [ 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ] );
	}

	/**
	 * Add Style and Scripts to admin page.
	 *
	 * @param string $hook_suffix Hook suffix page.
	 */
	public function add_script_to_admin_page( string $hook_suffix ): void {
		if ( 'toplevel_page_iwp-info-table' === $hook_suffix ) {
			wp_enqueue_style( 'admin-style', IWP_TEST_PLUGIN_URL . '/assets/css/admin.css', '', $this->version( '/assets/css/admin.css' ) );
			wp_enqueue_script( 'admin-main', IWP_TEST_PLUGIN_URL . '/assets/js/admin.js', [ 'jquery' ], $this->version( '/assets/js/admin.js' ), true );
			wp_enqueue_script( 'sweetalert2', '//cdn.jsdelivr.net/npm/sweetalert2@11', [ 'jquery' ], '2.0.0', true );
			wp_localize_script(
				'admin-main',
				'iwp',
				[
					'ajaxUrl' => admin_url( 'admin-ajax.php' ),
					'nonce'   => wp_create_nonce( 'amazing_table' ),
				]
			);
		}

	}


	/**
	 * Output html template.
	 *
	 * @return false|string
	 */
	public function output_shortcode() {
		ob_start();
		include IWP_TEST_PLUGIN_DIR . '/template/shortcodes/iwp_amazing_table.php';

		return ob_get_clean();
	}

	/**
	 * Get control version file.
	 *
	 * @param string $src Patch to file.
	 *
	 * @return false|int
	 */
	public function version( string $src ) {
		return filemtime( IWP_TEST_PLUGIN_DIR . $src );
	}

	/**
	 * Activation Plugin function.
	 * Add time settings.
	 */
	public function activation(): void {
		add_option( 'iwp_time_requests', HOUR_IN_SECONDS );
	}
}
