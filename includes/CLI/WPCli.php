<?php
/**
 * Created 20.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\CLI
 */

namespace IWP\CLI;

use IWP\API\Api;
use WP_CLI_Command;

/**
 * Class IWPWpCli
 *
 * @package IWP
 */
class WPCli extends WP_CLI_Command {
	/**
	 * Available time constants
	 *
	 * @var string[]
	 */
	protected $time_maker = [
		'MINUTE_IN_SECONDS',
		'HOUR_IN_SECONDS',
		'DAY_IN_SECONDS',
		'WEEK_IN_SECONDS',
		'YEAR_IN_SECONDS',
	];

	/**
	 * Works with awesomemotive test API.
	 *
	 * @var Api
	 */
	protected $api;

	/**
	 * Option time update information table.
	 *
	 * @var false|mixed|void
	 */
	protected $time_update_table;

	/**
	 * IWPWpCli constructor.
	 */
	public function __construct() {
		parent::__construct();

		$this->time_update_table = get_option( 'iwp_time_requests', true );
		$this->api               = new Api();
	}

	/**
	 * Change Time request to API.
	 *
	 * ## EXAMPLES
	 *
	 *     $ wp iwp change_time MINUTE_IN_SECONDS
	 *     Success: Change Success.
	 *     :Updates data once an minute
	 *
	 *     $ wp iwp change_time HOUR_IN_SECONDS
	 *     Success: Change Success.
	 *     :Updates data once an hour (default value)
	 *
	 *     $ wp iwp change_time DAY_IN_SECONDS
	 *     Success: Change Success.
	 *     :Updates data once an day
	 *
	 *     $ wp iwp change_time WEEK_IN_SECONDS
	 *     Success: Change Success.
	 *     :Updates data once an week
	 *
	 *     $ wp iwp change_time YEAR_IN_SECONDS
	 *     Success: Change Success.
	 *     :Updates data once an year
	 *
	 * <Time>
	 *      :Available options
	 *      MINUTE_IN_SECONDS
	 *      HOUR_IN_SECONDS Default.
	 *      DAY_IN_SECONDS
	 *      WEEK_IN_SECONDS
	 *      YEAR_IN_SECONDS
	 *
	 * @subcommand   change_time
	 *
	 * @param array $args Arguments.
	 */
	public function change_time( $args ): void {
		if ( empty( $args[0] ) ) {
			\WP_CLI::error( __( 'The TIME parameter is not specified, the available parameters can be viewed in the --help', 'iwp' ) );
		}

		if ( ! in_array( $args[0], $this->time_maker, true ) ) {
			\WP_CLI::error( __( 'Invalid TIME parameter, the available parameters can be viewed in the --help', 'iwp' ) );
		}

		/**
		 * Set Options time and update information.
		 */
		$options = update_option( 'iwp_time_requests', constant( $args[0] ) );
		delete_transient( 'iwp_tables' );

		$this->api->get_api_request();

		if ( ! $options ) {
			\WP_CLI::warning( "Option not applied '$args[0]'. or you set a value that is already" );
		}

		\WP_CLI::success( __( 'Change Success', 'iwp' ) );
	}

	/**
	 * Refreshing data in a table from shortcode [iwp_amazing_table].
	 *
	 * ## EXAMPLES
	 *
	 *     $ wp iwp update_info
	 *     Success: Change Success.
	 *
	 * @subcommand update_info
	 */
	public function update_info(): void {
		// delete old transient.
		delete_transient( 'iwp_tables' );
		// set life time data one second.
		update_option( 'iwp_time_requests', 1 );
		$response = $this->api->get_api_request();

		if ( false === $response ) {
			\WP_CLI::error( __( 'No response from Api', 'iwp' ) );
		}

		if ( is_wp_error( $response ) ) {
			\WP_CLI::error( $response->get_error_messages() );
		}

		\WP_CLI::success( __( 'Update Success', 'iwp' ) );
	}
}
