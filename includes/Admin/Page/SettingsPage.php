<?php
/**
 * Created 20.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Admin\Page
 */

namespace IWP\Admin\Page;

/**
 * Class IWPSettingsPage
 *
 * @package IWP
 */
class SettingsPage {
	/**
	 * IWPSettingsPage constructor.
	 */
	public function __construct() {
		add_action( 'admin_menu', [ $this, 'add_admin_menu' ] );
		add_action( 'admin_init', [ $this, 'add_section_page' ] );
	}

	/**
	 * Add menu page.
	 */
	public function add_admin_menu(): void {
		add_menu_page(
			__( 'Amazing table', 'iwp' ),
			__( 'Amazing table', 'iwp' ),
			'manage_options',
			'iwp-info-table',
			[ $this, 'template_admin_page' ],
			'dashicons-list-view',
			6
		);
	}

	/**
	 * Output template page.
	 */
	public function template_admin_page(): void {
		include_once IWP_TEST_PLUGIN_DIR . '/template/settings-page/options.php';
	}

	/**
	 * Add section in admin page
	 */
	public function add_section_page() {
		add_settings_section( 'test-part-one-header', '', [ $this, 'header_section' ], 'iwp-info-table' );
		add_settings_section( 'test-part-one-content', '', [ $this, 'table_content' ], 'iwp-info-table' );
	}

	/**
	 * Output Header section HTML.
	 */
	public function header_section(): void {
		?>
		<div id="test-part-one-header">
			<img src="<?php echo esc_url( IWP_TEST_PLUGIN_URL . '/assets/img/logo.png' ); ?>" alt="Some Logo">
		</div>
		<?php
	}

	/**
	 * Output Content section HTML.
	 */
	public function table_content(): void {
		include_once IWP_TEST_PLUGIN_DIR . '/template/settings-page/table.php';
	}
}
