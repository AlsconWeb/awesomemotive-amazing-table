===  Awesomemotive Amazing table ===
Author: Alex L
Author URI: https://gitlab.com/AlsconWeb
Tags: awesomemotive, test work
Contributors: alexls
Requires PHP: 7.4.21
Tested up to: 7.4.21
Stable tag: 1.0.0

Requires WordPress Version at least: 5.1

simple plugin

== Description ==
= WP_CLI command =
Change Time request to API.

= EXAMPLES =
 $ wp iwp change_time MINUTE_IN_SECONDS
 Updates data once an minute

 $ wp iwp change_time HOUR_IN_SECONDS
 Updates data once an hour (default value)

 $ wp iwp change_time DAY_IN_SECONDS
 Updates data once an day

 $ wp iwp change_time WEEK_IN_SECONDS
 Updates data once an week

 $ wp iwp change_time YEAR_IN_SECONDS
 Updates data once an year

Refreshing data in a table from shortcode [iwp_amazing_table].

= EXAMPLES =
 $ wp iwp update_info

== Installation ==

To install this plugin, please refer to the guide here: [http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation](http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation)

== Changelog ==

= 1.0 =
* First release.